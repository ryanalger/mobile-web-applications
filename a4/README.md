# LIS4381 - Mobile Web Applications Development

## Ryan Alger

### Assignment 4 Requirements:

*Three Parts:*

1. Clone starter files
2. Modify meta tags and navigation links
3. Work with bootstrap carousel
4. Perform basic client-side data validation using regex

#### Assignment Screenshots:

*Screenshot of Home Page:*  

![Main Interface](img/home.png)

*Screenshot of Invalid Entries:*  

![Main Interface](img/invalid.png)

*Screenshot of Valid Entries:*

![Recipe Interface](img/valid.png)

