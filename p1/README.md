# LIS4381 - Mobile Web Applications Development

## Ryan Alger

### Project 1 Requirements:

*Four Parts:*

1. Create Launcher Icon
2. Create First User Interface
3. Create Second User Interface
4. Link Both Interfaces via 'Details' Button


#### Assignment Screenshots:

*Screenshot of First Interface:*  

![Main Interface](img/ActivityMain.png)

*Screenshot of Second Interface:*

![Recipe Interface](img/ActivityDetails.png)

