# LIS4381 - Mobile Web Applications Development

## Ryan Alger

### Assignment 2 Requirements:

*Three Parts:*

1. Create First User Interface
2. Create Second User Interface
3. Link Both Interfaces via 'View Recipe' Button

#### Assignment Screenshots:

*Screenshot of First Interface:*  

![Main Interface](img/main.png)

*Screenshot of Second Interface:*

![Recipe Interface](img/recipe.png)

