<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My A1 page">
	<meta name="author" content="Ryan Alger">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 1</title>
	<?php $thisPage = "A1"; ?>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<?php include_once("info/header.php"); ?>	
					</div>

					<h3>Four Parts:</h3>
					<p>Distributed Version Control with Git and Bitbucket</p>
					<p>Development Installations</p>
					<p>Git Commands</p>
					<p>Bitbucket Tutorial Repo Links</p>
					
					<h3>Git Commands w/ Short Descriptions:</h3>
					<p>git init - Create an empty git repository or reinitialize an existing one</p>
					<p>git status - Show the working tree status</p>
					<p>git add - Add file contents to the index</p>
					<p>git commit - Record changes to the repository</p>
					<p>git push - Update remote refs along with associated objects</p>
					<p>git pull - Fetch from and merge with another repository or local branch</p>
					<p>git log - Shows a listing of commits on a branch including the corresponding details</p>

					<h3>Assignment Screenshots:</h2>

					<h4><i>Screenshot of my AMMPS Installation</i></h4>
					<img src="img/phpInstall.png" alt="PHP Installation">
					<br>
					<br>

					<h4><i>Screenshot of Java Hello</i></h4>
					<img src="img/javaHello.png" alt="Java Hello">
					<br>
					<br>

					<h4><i>Screenshot of Android Studio Test App</i></h4>
					<img src="img/myFirstApp.png" alt="First App">

					
			<br>
			<br>
			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- Turn off client-side validation, in order to test server-side validation.  -->
<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>

<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
