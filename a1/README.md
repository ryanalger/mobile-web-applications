# LIS4381 - Mobile Web Applications Development

## Ryan Alger

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Git Commands 
4. Bitbucket Tutorial Repo Links

#### Git Commands w/ Short Descriptions:

1. git init - Create an empty git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and merge with another repository or local branch
7. git log - Shows a listing of commits on a branch including the corresponding details

#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation:](http://localhost:81/cgi-bin/phpinfo.cgi)*   

![PHP Install](img/phpInstall.png)

*Screenshot of Java Hello:*

![Java Hello](img/javaHello.png)

*Screenshot of Android Studio - My First App:*

![Java Hello](img/myFirstApp.png)

#### Bitbucket Tutorial Links:

*Tutorial - Station Locations:*
[A1: Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ryanalger/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial -  Request to update a teammate's repository:*
[A1: My Team Quotes Tutorial Link](https://bitbucket.org/ryanalger/myteamquotes/ "My Team Quotes Tutorial")