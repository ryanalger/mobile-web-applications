<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
$pst_name_v = $_POST['name'];
$pst_street_v = $_POST['street'];
$pst_city_v = $_POST['city'];
$pst_state_v = $_POST['state'];
$pst_zip_v = $_POST['zip'];
$pst_phone_v = $_POST['phone'];
$pst_email_v = $_POST['email'];
$pst_url_v = $_POST['url'];
$pst_ytdsales_v = $_POST['ytdsales'];
$pst_notes_v = $_POST['notes'];
//exit(print_r($_POST));

//get item data

//code to process inserts goes here
//name: letters, numbers, hyphens and underscore
$pattern ='/^[a-zA-Z0-9\-_\s]+$/';
$valid_name = preg_match($pattern, $pst_name_v);
//echo 'name is ' . $valid_name;

//street: letters, comma, space and period
$pattern ='/^[a-zA-Z0-9,\s\.]+$/';
$valid_street = preg_match($pattern, $pst_street_v);
//echo ' street is ' . $valid_street;

//city: letters and space
$pattern ='/^[a-zA-Z0-9\-\s]+$/';
$valid_city = preg_match($pattern, $pst_city_v);
//echo ' city is ' . $valid_city;

//state: 2 letters
$pattern ='/^[a-zA-Z]{2,2}+$/';
$valid_state = preg_match($pattern, $pst_state_v);
//echo ' state is ' . $valid_state;

//zip: 5 or 9 numbers
$pattern='/^\d{5,9}+$/';
$valid_zip= preg_match($pattern, $pst_zip_v);
//echo ' zip is ' . $valid_zip;

//phone: 10 numbers
$pattern='/^\d{10}+$/';
$valid_phone= preg_match($pattern, $pst_phone_v);
//echo ' phone is ' . $valid_phone;

//email
$pattern='/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
$valid_email= preg_match($pattern, $pst_email_v);
//echo ' email is ' . $valid_email;

//url
$pattern='/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
$valid_url= preg_match($pattern, $pst_url_v);
//echo ' URL is ' . $valid_url;

//ytd sales: 
$pattern='/^\d{1,8}(?:\.\d{0,2})?$/';
$valid_ytdsales= preg_match($pattern, $pst_ytdsales_v);
//echo ' YTD is ' . $valid_ytdsales;
//exit();

//validate input
if 
(
    empty($pst_name_v) || 
    empty($pst_street_v) || 
    empty($pst_city_v) || 
    empty($pst_zip_v) || 
    empty($pst_phone_v) || 
    empty($pst_email_v) || 
    empty($pst_url_v) || 
    !isset($pst_ytdsales_v)
)
{
    $error = "All fields require data except for <b>notes</b>. Check all fields and try again.";
    include('global/error.php');
    exit();
}

else if (!is_numeric($pst_ytdsales_v) || $pst_ytdsales_v < 0)
{
    $error = "YTD Sales can only contain numbers (and a decimal point) and must be equal to or greater than 0.";
    include('global/error.php');
    exit();
}

else if ($valid_name === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_name === 0)
{
    $error = "Name can only contain letters, numbers, hyphens, underscores and spaces.";
    include('global/error.php');
    exit();
    
}

else if ($valid_street === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_street === 0)
{
    $error = "Street can only contain letters, numbers, commas and periods.";
    include('global/error.php');
    exit();
}

else if ($valid_city === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_city === 0)
{
    $error = "City can only contain letters, numbers, spaces and hyphens.";
    include('global/error.php');
    exit();
}

else if ($valid_state === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_state === 0)
{
    $error = "State can only contain 2 letters.";
    include('global/error.php');
    exit();
}

else if ($valid_zip === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_zip === 0)
{
    $error = "Zip must contain 5 - 9 digits and no other characters.";
    include('global/error.php');
    exit();
}

else if ($valid_phone === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_phone === 0)
{
    $error = "Phone must contain 10 digits and no other characters.";
    include('global/error.php');
    exit();
}

else if ($valid_email === false)
{
    echo 'Error in pattern!';
}

else if ($valid_email === 0)
{
    $error = "Must enter a valid email.";
    include('global/error.php');
    exit();
}

else if ($valid_url === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_url === 0)
{
    $error = "Must enter a valid URL.";
    include('global/error.php');
    exit();
}

else if ($valid_ytdsales === false)
{
    echo 'Error in pattern!';
    exit();
}

else if ($valid_ytdsales === 0)
{
    $error = "YTD Sales must contain no more than 10 digits, including the decimal point.";
    include('global/error.php');
    exit();
}

else
{
    require_once('global/connection.php');
}

$query = 
"insert into petstore
(pst_name, pst_street, pst_city, pst_state, pst_zip, pst_phone, pst_email, pst_url, pst_ytd_sales, pst_notes)
values
(:pst_name_p, :pst_street_p, :pst_city_p, :pst_state_p, :pst_zip_p, :pst_phone_p, :pst_email_p, :pst_url_p, :pst_ytd_sales_p, :pst_notes_p)";

try
    {
        $statement = $db->prepare($query);
        $statement->bindParam(':pst_name_p', $pst_name_v);
        $statement->bindParam(':pst_street_p', $pst_street_v);
        $statement->bindParam(':pst_city_p', $pst_city_v);
        $statement->bindParam(':pst_state_p', $pst_state_v);
        $statement->bindParam(':pst_zip_p', $pst_zip_v);
        $statement->bindParam(':pst_phone_p', $pst_phone_v);
        $statement->bindParam(':pst_email_p', $pst_email_v);
        $statement->bindParam(':pst_url_p', $pst_url_v);
        $statement->bindParam(':pst_ytd_sales_p', $pst_ytdsales_v);
        $statement->bindParam(':pst_notes_p', $pst_notes_v);
        $statement->execute();
        $statement->closeCursor();

        $last_auto_increment_id = $db->lastInsertId();
        //include('index.php'); //forwarding is faster, one trip to server
        header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
    }

    catch(PDOException $e)
    {
        $error = $e->getMessage();
        echo $error;
    }
?>
