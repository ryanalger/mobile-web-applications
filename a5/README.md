# LIS4381 - Mobile Web Applications Development

## Ryan Alger

### Assignment 5 Requirements:

*Three Parts:*

1. Use cloned A4 Files
2. Review subdirectories and files
3. Process and validate user input

#### Assignment Screenshots:

*Screenshot of index.php:*  

![Main Interface](img/index_page.png)

*Screenshot of Error Page:*  

![Main Interface](img/error_page.png)

*Screenshot of Adding a Petstore:*

![Recipe Interface](img/add_a_store.png)

*Screenshot of Added Store:*  

![Main Interface](img/store_added.png)

*Screenshot of Changes to MySQL DB:*  

![Main Interface](img/changes_to_db.png)
