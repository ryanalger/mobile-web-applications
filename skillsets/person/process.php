<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Displays the created Person objects">
	<meta name="author" content="Ryan Alger">
	<link rel="icon" href="favicon.ico">

	<title>PHP Person Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../nav/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("info/header.php"); ?>	
						</div>

						<h2>People</h2>

<a href="index.php">Add a Person</a>
<br />

 <div class="table-responsive">
	 <table id="myTable" class="table table-striped table-condensed" >

		 <!-- Code displaying PetStore data with Edit/Delete buttons goes here // -->

	 </table>
 </div> <!-- end table-responsive -->
 	
<?php
require_once("class_person.php");

$fname = $_POST['fname'];
$lname = $_POST['lname'];
$age = $_POST['age'];

$person1 = new Person();
$person2 = new Person($fname, $lname, $age);
?>


		</div> <!-- starter-template -->
  </div> <!-- end container -->

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
	
	<div class="table-responsive">
	<table id="myTable" class="table table-striped table-condensed" >
	<thead>
		<tr>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Age</th>
		</tr> 
	</thead>

	<tr>
	<td><?php echo $person1->GetFname();?></td>
	<td><?php echo $person1->GetLname();?></td>
	<td><?php echo $person1->GetAge();?></td>
	</tr>

	<tr>
	<td><?php echo $person2->GetFname();?></td>
	<td><?php echo $person2->GetLname();?></td>
	<td><?php echo $person2->GetAge();?></td>
	</tr>
	
	</table>
	</div>
</body>
</html>
