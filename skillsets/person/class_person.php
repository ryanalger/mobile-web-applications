<?php
    class Person
    {
        private $fname;
        private $lname;
        private $age;

        //constructor
        public function __construct($fn = "Joe", $ln = "Doe", $a = 33)
        {
            $this->fname = $fn;
            $this->lname = $ln;
            $this->age = $a;

            echo "Creating <b> ";
            echo $this->fname;
            echo " ";
            echo $this->lname;
            echo "</b> is <b>";
            echo $this->age;
            echo "</b> person object from parameterized constructor (accepts 3 args) <br>";
        }

        //destructor
        public function __destruct()
        {
            echo "Destroying <b>";
            echo $this->fname;
            echo " ";
            echo $this->lname;
            echo "</b> is <b>";
            echo $this->age;
            echo "</b> person object. <br>";
        }

        //set em
        public function SetFname($fn) 
        {
            $this->fname = $fn;
        }

        public function SetLname($ln) 
        {
            $this->lname = $ln;
        }

        public function SetAge($a) 
        {
            $this->age = $a;
        }

        //get em
        public function GetFname() 
        {
            return $this->fname;
        }

        public function GetLname() 
        {
            return $this->lname;
        }

        public function GetAge() 
        {
            return $this->age;
        }
    }
?>