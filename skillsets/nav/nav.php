	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="../../index.php">Home</a></li>
					<li><a href="../../a1/index.php">A1</a></li>
					<li><a href="../../a2/index.php">A2</a></li>
					<li><a href="../../a3/index.php">A3</a></li>
					<li><a href="../../a4/index.php">A4</a></li>
					<li><a href="../../a5/index.php">A5</a></li>
					<li><a href="../../p1/index.php">P1</a></li>
					<li><a href="../../p2/index.php">P2</a></li>
					<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Skillsets
						<span class="caret"></span></a>
						<ul class="dropdown-menu">
						<li><a href="../../skillsets/calc/index.php">Calculator</a></li>
						<li><a href="../../skillsets/person/index.php">Person Class</a></li>
						<li><a href="../../skillsets/employee/index.php">Employee Class</a></li>
						<li><a href="../../skillsets/write/index.php">File Manipulation</a></li>
						<li><a href="../../skillsets/rss/index.php">RSS Feed Integration</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>