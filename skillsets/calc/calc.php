<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Adds, subtracts, multiplies and divides">
	<meta name="author" content="Ryan Alger">
	<link rel="icon" href="favicon.ico">

	<title>PHP Calculator: Results</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../nav/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

    <?php
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $mathOp = $_POST["mathOp"];

        if ($mathOp == "add")
        {
            addition($num1, $num2);
        }

        else if ($mathOp == "sub")
        {
            subtraction($num1, $num2);
        }

        else if ($mathOp == "mult")
        {
            multiplication($num1, $num2);
        }

        else if ($mathOp == "div")
        {
            division($num1, $num2);
        }

        else if ($mathOp == "exp")
        {
            exponentiation($num1, $num2);
        }


        function addition($num1, $num2)
        {
            echo "<h2>Addition</h2>";
            echo $num1 . " + " . $num2 . " = ";
            echo $num1 + $num2;
        }

        function subtraction($num1, $num2)
        {
            echo "<h2>Subtraction</h2>";
            echo $num1 . " - " . $num2 . " = ";
            echo $num1 - $num2;
        }

        function multiplication($num1, $num2)
        {
            echo "<h2>Multiplication</h2>";
            echo $num1 . " * " . $num2 . " = ";
            echo $num1 * $num2;
        }

        function division($num1, $num2)
        {
            echo "<h2>Division</h2>";
            if ($num2 == 0)
            {
                exit("Cannot divide by zero...");
            }
            echo $num1 . " / " . $num2 . " = ";
            echo $num1 / $num2;
        }

        function exponentiation($num1, $num2)
        {
            echo "<h2>Exponentiation</h2>";
            echo $num1 . " ^ " . $num2 . " = ";
            echo pow ($num1, $num2);
        }
    ?>

<br>
		<?php include_once "../../global/footer.php"; ?>

			
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->
</body>
</html>