<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="a from for the user to interact with the PHP calculator">
	<meta name="author" content="Ryan Alger">
	<link rel="icon" href="favicon.ico">

	<title>PHP Calculator</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../nav/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Perform Calculation</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="calc.php">
								<div class="form-group">
										<label class="col-sm-1 control-label">Num1:</label>
										<div class="col-sm-10">
												<input type="text" class="form-control" name="num1" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-1 control-label">Num2:</label>
										<div class="col-sm-10">
												<input type="text" class="form-control" name="num2" />
										</div>
								</div>

								<label class="radio-inline">
      								<input type="radio" name="mathOp" value="add">Addition</label>
								<label class="radio-inline">
									<input type="radio" name="mathOp" value="sub">Subtraction</label>
								<label class="radio-inline">	
									<input type="radio" name="mathOp" value="mult">Multiplication</label>
								<label class="radio-inline">
									<input type="radio" name="mathOp" value="div">Division</label>
								<label class="radio-inline">
									<input type="radio" name="mathOp" value="exp">Exponentiation</label>

								<br>
								<br>

								<div class="form-group">
										<div class="col-sm-6 col-sm-offset-3">
												<button type="submit" class="btn btn-primary" name="calc" value="Calculate">Calculate</button>
										</div>
								</div>
						</form>
					</div>
			</div>

			<?php include_once "../../global/footer.php"; ?>

			
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->
 	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- Turn off client-side validation, in order to test server-side validation.  -->
<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>

<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
