<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Process employee data">
	<meta name="author" content="Ryan Alger">
	<link rel="icon" href="favicon.ico">

	<title>Employee Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../nav/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("info/header.php"); ?>	
						</div>

						<h2>Employees</h2>

<a href="index.php">Add an Employee</a>
<br />

 <div class="table-responsive">
	 <table id="myTable" class="table table-striped table-condensed">
	 </table>
 </div>
 	
<?php
	require_once("classes/class_person.php");
	require_once("classes/class_employee.php");

	$empFname = $_POST['fname'];
	$empLname = $_POST['lname'];
	$empAge = $_POST['age'];

	$empSSN = $_POST['ssn'];
	$empGender = $_POST['gender'];

	$emp1 = new Employee();
	$emp2 = new Employee($empFname, $empLname, $empAge, $empSSN, $empGender);
?>


		</div> <!-- starter-template -->
  </div> <!-- end container -->

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
	
	<div class="table-responsive">
	<table id="myTable" class="table table-striped table-condensed" >
	<thead>
		<tr>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Age</th>
		<th>SSN</th>
		<th>Gender</th>
		</tr> 
	</thead>

	<tr>
	<td><?php echo $emp1->GetFname();?></td>
	<td><?php echo $emp1->GetLname();?></td>
	<td><?php echo $emp1->GetAge();?></td>
	<td><?php echo $emp1->GetSSN();?></td>
	<td><?php echo $emp1->GetGender();?></td>
	</tr>

	<tr>
	<td><?php echo $emp2->GetFname();?></td>
	<td><?php echo $emp2->GetLname();?></td>
	<td><?php echo $emp2->GetAge();?></td>
	<td><?php echo $emp2->GetSSN();?></td>
	<td><?php echo $emp2->GetGender();?></td>
	</tr>
	
	</table>
	</div>
</body>
</html>
