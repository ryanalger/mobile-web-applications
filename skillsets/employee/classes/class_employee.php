<?php
    class Employee extends Person
    {
        private $ssn;
        private $gender;

        //constructor
        public function __construct($fn = "Jerry", $ln = "Bary", $a = 33, $s = '123-45-6789', $g = 'male')
        {
            $this->ssn = $s;
            $this->gender = $g;

            parent::__construct($fn, $ln, $a);

            echo("Creating <strong>" . person::GetFname() .  " " . person::GetLname() . "</strong> is <strong>" . person::GetAge() 
                    . "</strong> with ssn <strong>" . $this->ssn . "</strong> and is <strong>" . $this->gender 
                    . "</strong> employee object from parameterized constructor (accepts 5 args):</br></br>");
        }

        //destructor
        public function __destruct()
        {
            parent::__destruct();
            echo("Destroying <strong>" . person::GetFname() .  " " . person::GetLname() . "</strong> is <strong>" . person::GetAge() 
            . "</strong> with ssn <strong>" . $this->ssn . "</strong> and is <strong>" . $this->gender 
            . "</strong> employee object.</br></br>");
        }

        //set em
        public function SetSSN($s = '111111111') 
        {
            $this->ssn = $s;
        }

        public function SetAge($a = '18') 
        {
            $this->age = $a;
        }

        //get em
        public function GetSSN() 
        {
            return $this->ssn;
        }

        public function GetGender() 
        {
            return $this->gender;
        }
    }
?>