<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Reads the file">
	<meta name="author" content="Ryan Alger">
	<link rel="icon" href="favicon.ico">

	<title>Read a File</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../nav/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("info/header.php"); ?>	
						</div>
 </div> <!-- end table-responsive -->

<?php	
	echo "<h3>File Contents:</h3>";
	$fileSize = filesize("file.txt");
	if ($fileSize == 0)
	{
		echo "<h4><i>The file is empty :(</i></h4>";
	}
	else
	{
		$myfile = fopen("file.txt", "r") or die("Unable to open file!");
		echo "<h4>";
		echo htmlspecialchars(fread($myfile,filesize("file.txt"))); //XSS avoided
		echo "</h4>";
		fclose($myfile);
	}
?>

<form id="defaultForm" method="post" class="form-horizontal" action="index.php">

		<div class="form-group">
				<div class="col-sm-5 col-sm-offset-0">
						<button type="submit" class="btn btn-primary">Previous Page</button>
				</div>
		</div>
</form > 

		</div> <!-- starter-template -->
  </div> <!-- end container -->
  
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
