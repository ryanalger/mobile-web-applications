# LIS4381 - Mobile Web Applications Development

## Ryan Alger

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Distributed Version Contorl w/ Git and Bitbucket
	- Display Screenshots of Installations
	- Complete Bitbucket Tutorials
	- Provide Git Command Descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create First User Interface
    - Create Second User Interface
    - Link Both Interfaces via 'View Recipe' Button

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Pet Store Database
    - Forward Engineer to local host
    - Create My Event App
	
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone starter files
    - Modify meta tags and navigation links
    - Work with bootstrap carousel
    - Perform basic client-side data validation using regex
	
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Use cloned A4 Files
    - Review subdirectories and files
    - Process and validate user input

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create Launcher Icon
    - Create First User Interface
    - Create Second User Interface
    - Link Both Interfaces via 'Details' Button
	
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Allow for CRUD Cabability
    - Create RSS Feed Viewer    
    - Update Homepage