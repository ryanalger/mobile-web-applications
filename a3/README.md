# LIS4381 - Mobile Applications Development

## Ryan Alger

### Assignment 3 Requirements:

*Goals:*

1. Create Pet Store Database
2. Forward Engineer to local host
3. Create My Event App 

#### Assignment Links:

1. [a3.sql](docs/a3.sql)
2. [a3.mwb](docs/a3.mwb)

#### Assignment Screenshots:

*Screenshot of Pet Store ERD*:

![Pet Store ERD](img/ERD.png)

*Screenshot of Main Screen*:

![Main Screen](img/main_screen.png)

*Screenshot of Computed Total Price*:

![Computed Total](img/total_price.png)


