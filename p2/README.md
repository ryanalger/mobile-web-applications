# LIS4381 - Mobile Web Applications Development

## Ryan Alger

### Project 2:

*Three Parts:*

1. Allow for CRUD Cabability
2. Create RSS Feed Viewer
3. Update Homepage

#### Assignment Screenshots:

*Screenshot of Data Table:*  

![Data Table](img/table.png)

*Screenshot of Successful Update:*  

![Updating Table](img/updateSuccess.png)

*Screenshot of Failed Update:*

![Update Failed](img/updateFailed.png)

*Screenshot of RSS Feed:*  

![RSS](img/rss.png)

*Screenshot of Changes to Homepage:*  

![Home](img/home.png)
