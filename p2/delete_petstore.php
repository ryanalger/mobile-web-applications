<?php
//show errors:
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//get item ID
$pst_id_v = $_POST["pst_id"];
//exit(print_r($_POST));

require_once('global/connection.php');

//delete item from the DB
$query = "delete from petstore where pst_id = :pst_id_p";

try
{
    $statement = $db->prepare($query);
    $statement->bindParam(':pst_id_p', $pst_id_v);
    $row_count = $statement->execute();
    $statement->closeCursor();

    //uncomment to view the affected rows:
    //exit($row_count);
    header('Location: index.php');
}

catch (PDOException $e)
{
    $error = $e->getMessage();
    echo $error;
}
?>
